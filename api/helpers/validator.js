const { validationResult, checkSchema } = require('express-validator');

const User = require('../models/User')

module.exports.validate = (req, res, next) => {
  const errors = validationResult(req)
  if (errors.isEmpty()) {
    return next();
  }
  return res.status(422).json({ success: false, errors: errors.mapped() });
}



/**Auth v1*/

module.exports.register = [

    checkSchema({

      //first name
      first_name: {
        notEmpty: {
          errorMessage: "First Name required",
        },
        isLength:{
          errorMessage: 'First Name must be at least 3 chars and less than 15 chars long',
          options:{
            min: 3,
            max: 15
          }
        },
      },

      //last name
      last_name: {
        notEmpty: {
          errorMessage: "Last Name required",
        },
        isLength: {
          errorMessage: 'Last Name must be at least 3 chars and less than 30 chars long',
          options:{
            min: 3,
            max: 30
          }
        },
      },

      //email
      email: {
        notEmpty: {
          errorMessage: "Email required",
        },
        isEmail: true,
        normalizeEmail: true,
        errorMessage: "Please enter a valid email",
        custom: {
          options: value => {
              return User.find({
                  email: value
              }).then(user => {
                  if (user.length > 0) {
                      return Promise.reject('Email address already taken')
                  }
              })
          }
        }
      },

      //password
      password: {
        notEmpty: {
          errorMessage: "Password required",
        },
        isStrongPassword: {
          minLength: 8,
          minLowercase: 1,
          minUppercase: 1,
          minNumbers: 1,
          errorMessage: 'Password must be greater than 8, and must include one lowercase character, one uppercase character, a number, and a special character'
        },
        // isStrongPassword: {
        //   matches: (/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{8,}$/, "i"),
        //   errorMessage: 'Password must be greater than 8, and must include one lowercase character, one uppercase character, a number, and a special character'
        // }
      },

      //password confirmation
      passwordConfirmation: {
        notEmpty: {
          errorMessage: "Password Confirmation required",
        },
        custom: {
          options: (value, { req }) => {
            if (value !== req.body.password) {
              throw new Error('Password confirmation does not match password');
            }
            return true;
          }
        }
      },

    }
  ),
]

module.exports.login = [
    checkSchema({

      //email
      email: {
        notEmpty: {
          errorMessage: "Email required",
        },
        isEmail: true,
        normalizeEmail: true,
        errorMessage: "Please enter a valid email",
      },

      //password
      password: {
        notEmpty: {
          errorMessage: "Password required",
        },
        isStrongPassword: {
          minLength: 8,
          minLowercase: 1,
          minUppercase: 1,
          minNumbers: 1,
          errorMessage: 'Password must be greater than 8, and must include one lowercase character, one uppercase character, a number, and a special character'
        },
      },
    }
  ),
]

/**User Management v1 */

module.exports.createUser = [

    checkSchema({

      //first name
      first_name: {
        notEmpty: {
          errorMessage: "First Name required",
        },
        isLength:{
          errorMessage: 'First Name must be at least 3 chars and less than 15 chars long',
          options:{
            min: 3,
            max: 15
          }
        },
      },

      //last name
      last_name: {
        notEmpty: {
          errorMessage: "Last Name required",
        },
        isLength: {
          errorMessage: 'Last Name must be at least 3 chars and less than 30 chars long',
          options:{
            min: 3,
            max: 30
          }
        },
      },

      //email
      email: {
        notEmpty: {
          errorMessage: "Email required",
        },
        isEmail: true,
        normalizeEmail: true,
        errorMessage: "Please enter a valid email",
        custom: {
          options: value => {
              return User.find({
                  email: value
              }).then(user => {
                  if (user.length > 0) {
                      return Promise.reject('Email address already taken')
                  }
              })
          }
        }
      },

      //password
      password: {
        notEmpty: {
          errorMessage: "Password required",
        },
        isStrongPassword: {
          minLength: 8,
          minLowercase: 1,
          minUppercase: 1,
          minNumbers: 1,
          errorMessage: 'Password must be greater than 8, and must include one lowercase character, one uppercase character, a number, and a special character'
        },
      },

      passwordConfirmation: {
        notEmpty: {
          errorMessage: "Password Confirmation required",
        },
        custom: {
          options: (value, { req }) => {
            if (value !== req.body.password) {
              throw new Error('Password confirmation does not match password');
            }
            return true;
          }
        }
      },

      //phone
      phone: {
        notEmpty: false,
        isNumeric: true,
        isMobilePhone: true,
        errorMessage: 'Please enter a valid phone number',
        custom: {
          options: value => {
              return User.find({
                  phone: value
              }).then(user => {
                  if (user.length > 0) {
                      return Promise.reject('Phone number already taken')
                  }
              })
          }
        }
      },

      //address
      address: {
        isLength:{
          errorMessage: 'Address cannot greater than 30 chars long',
          options: {
            max: 30
          }
        }
      },
    }
  ),

]

module.exports.updateUser = [

    checkSchema({

      //first name
      first_name: {
        notEmpty: {
          errorMessage: "First Name required",
        },
        isLength:{
          errorMessage: 'First Name must be at least 3 chars and less than 15 chars long',
          options:{
            min: 3,
            max: 15
          }
        },
      },

      //last name
      last_name: {
        notEmpty: {
          errorMessage: "Last Name required",
        },
        isLength: {
          errorMessage: 'Last Name must be at least 3 chars and less than 30 chars long',
          options:{
            min: 3,
            max: 30
          }
        },
      },

      //email
      email: {
        notEmpty: {
          errorMessage: "Email required",
        },
        isEmail: true,
        normalizeEmail: true,
        errorMessage: "Please enter a valid email",
        custom: {
          options: (value, {req}) => {
              return User.findOne({ email:value, _id:{ $ne: req.params.id } })
              .then(user => {
                  if (user !== null) {
                      return Promise.reject('Email address already taken')
                  }
              })
          }
        }
      },

      // //password
      // password: {
      //   notEmpty: {
      //     errorMessage: "Password required",
      //   },
      //   isStrongPassword: {
      //     minLength: 8,
      //     minLowercase: 1,
      //     minUppercase: 1,
      //     minNumbers: 1,
      //     errorMessage: 'Password must be greater than 8, and must include one lowercase character, one uppercase character, a number, and a special character'
      //   },
      // },

      // passwordConfirmation: {
      //   custom: {
      //     options: (value, { req }) => {
      //       if (value !== req.body.password) {
      //         throw new Error('Password confirmation does not match password');
      //       }
      //       return true;
      //     }
      //   }
      // },

      //phone
      phone: {
        notEmpty: false,
        isNumeric: true,
        isMobilePhone: true,
        errorMessage: 'Please enter a valid phone number',
        custom: {
          options: (value, {req}) => {
              return User.findOne({ phone:value, _id:{ $ne: req.params.id } })
              .then(user => {
                  if (user !== null) {
                      return Promise.reject('Phone number already taken')
                  }
              })
          }
        }
      },

      //address
      address: {
        isLength:{
          errorMessage: 'Address cannot greater than 30 chars long',
          options: {
            max: 30
          }
        }
      },
    }
  ),

]
