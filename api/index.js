const express = require('express')
const logger = require('morgan')

//Require MongoDB config
const db = require('./config/db')

//Create express instance
const app = express()

//logging
app.use(logger('dev'))

// Init body-parser options (inbuilt with express)
app.use(express.json());
app.use(express.urlencoded({ extended : true}));

//Require & Import API routes
const auth = require('./routes/auth')
const users = require('./routes/users')

//Use API Routes
app.use(auth)
app.use(users)

//Export the server middleware
module.exports = {
  path: '/api',
  handler: app
}



