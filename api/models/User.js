const { Schema, model } = require('mongoose')

const UserSchema = new Schema(
  {
    //first name
    first_name: {
      type: String,
      required: true,
    },

    //last name
    last_name: {
      type: String,
      required: true,
    },

    //email
    email: {
      type: String,
      index: {
        unique: true
      }
    },

    //password
    password: {
      type: String,
      required: true,
    },

    //phone
    phone: {
      type: String,
      required: false,
      unique: true,
      sparse: true,
    },

    //address
    address: {
      type: String,
      required: false
    },
  },

  //timestamps
  {
    timestamps: {
      createdAt: 'created_at',
      updatedAt: 'updated_at'
    }
  }

)

module.exports = model('User', UserSchema)

