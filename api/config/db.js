const mongoose = require('mongoose')

// Mongoose create the db automatically. We don't need to create DB from mongodb terminal.

// Set up default connection
mongoose.connect(`mongodb://${process.env.MONGO_HOSTNAME}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  // useFindAndModify: false,
  // useCreateIndex:true
})

//Get the default connection
var db = mongoose.connection;

//Bind connection to success event
db.once('open', () => {
  console.log('MongoDB : Connected Successfully ..');
})

//Bind connection to error event (to get notification of connection errors)
db.on('error', (err) => {
  console.log(`MongoDB Connection Error : ${err}`);
})

module.exports = db


