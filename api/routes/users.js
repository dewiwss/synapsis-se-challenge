const authConfig = require('../config/jwt')
const { Router } = require('express')

const router = Router()

// Initialize Controller
const usersController = require('../controllers/v1/usersController')

//validate
const validator = require('../helpers/validator')

// Get All Users
router.get('/v1/users', authConfig.isAuthenticated, usersController.list)

// Get Specific User
router.get('/v1/users/:id', authConfig.isAuthenticated, usersController.show)

// Add New User
router.post('/v1/users', authConfig.isAuthenticated, validator.createUser, validator.validate, usersController.create)

// Update User
router.patch('/v1/users/:id', authConfig.isAuthenticated, validator.updateUser, validator.validate, usersController.update)

// Delete User
router.delete('/v1/users/:id', authConfig.isAuthenticated, usersController.destroy)

module.exports = router
