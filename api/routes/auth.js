const { Router } = require('express')

const router = Router()

// Initialize Controller
const authController = require('../controllers/v1/authController')

// validate
const validator = require('../helpers/validator')

// Register
router.post('/v1/auth/register', validator.register, validator.validate, authController.register)

// Login
router.post('/v1/auth/login', validator.login, validator.validate, authController.login)

// Get User itself
router.get('/v1/auth/user', authController.user)

module.exports = router
