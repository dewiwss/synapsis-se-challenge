const path = require('path')

const authConfig = require(path.resolve('__dirname','../api/config/jwt'))
const User = require(path.resolve('__dirname','../api/models/User'))

const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')


/* Register */
module.exports.register = [

  (req, res) => {

      // initialize record
      let user = new User({
        first_name : req.body.first_name,
        last_name : req.body.last_name,
        email : req.body.email,
        password : req.body.password,
      });

      // encrypt password
      let salt = bcrypt.genSaltSync(10);
      let hash = bcrypt.hashSync(user.password, salt);
      user.password = hash

      // save record
      user.save(function(err, user){
          if(err) {
              return res.status(500).json({
                  success: false,
                  message: 'Error saving record',
                  error: err
              });
          }
          return res.json({
              success: true,
              message: 'user data saved successfully',
              _id: user._id
          });
      })
    }
]


/* Login */
module.exports.login = [

  (req, res) => {

      // validate email and password are correct
      User.findOne({email: req.body.email}, (err, user) => {
          if(err) {
              return res.status(500).json({
                  success: false,
                  message: 'Error logging in',
                  error: err
              });
          }

          if (user === null) {
            return res.status(500).json({
              success: false,
              message: 'Email address you entered is not found.'
            });
          }

          // compare submitted password with password inside db
          return bcrypt.compare(req.body.password, user.password, (err, isMatched) => {
            if(isMatched===true){
              return res.json({
                success: true,
                user: {
                  _id: user._id,
                  email: user.email,
                  first_name: user.first_name,
                  last_name: user.last_name
                },
                token: jwt.sign({_id: user._id, email: user.email, first_name: user.first_name, last_name: user.last_name}, authConfig.authSecret) // generate JWT token here
              });
            }
            else{
              return res.status(500).json({
                success: false,
                message: 'Invalid Email or Password entered.'
              });
            }
          });
      });
  }
]

/* Show User itself */
module.exports.user = (req, res) => {
  let token = req.headers.authorization
  if (token) {
    // verifies secret and checks if the token is expired
    jwt.verify(token.replace(/^Bearer\s/, ''), authConfig.authSecret, (err, decoded) => {
      if (err) {
        return res.status(401).json({success: false, message: 'unauthorized'})
      } else {
        return res.json({ success: true, user: decoded })
      }
    });
  }
  else{
    return res.status(401).json({ success: false, message: 'unauthorized'})
  }
}


