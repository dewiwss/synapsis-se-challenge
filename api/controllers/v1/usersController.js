const path = require('path')

const User = require(path.resolve('__dirname','../api/models/User'))

const bcrypt = require('bcryptjs')


/* Get All Users */
module.exports.list = (req, res, next) => {
  User.find({}, (err, users) => {
    if(err) {
        return res.status(500).json({
            success: false,
            message: 'Error getting records.'
        });
    }
    return res.json({success: true, users:users});
  });
}


/* Get Specific User by id*/
module.exports.show = (req, res) => {
  let id = req.params.id;
  User.findOne({_id: id}, (err, user) => {
      if(err) {
          return res.status(500).json({
              success: false,
              message: 'Error getting record.'
          });
      }
      if(!user) {
          return res.status(404).json({
              success: false,
              message: 'No such record'
          });
      }
      // return res.json(user);
      return res.json({success: true, user:user});
    });
}

/* Add New User */
module.exports.create = [


  (req, res) => {

      // initialize record
      let user = new User({
        first_name : req.body.first_name,
        last_name : req.body.last_name,
        email : req.body.email,
        password : req.body.password,
        phone : req.body.phone,
        address : req.body.address,
      })

      // encrypt password
      var salt = bcrypt.genSaltSync(10);
      var hash = bcrypt.hashSync(user.password, salt);
      user.password = hash

      // save record
      user.save((err, user) => {
          if(err) {
              return res.status(500).json({
                  success: false,
                  message: 'Error saving record',
                  error: err
              });
          }
          return res.status(201).json({
              success: true,
              message: 'user data saved successfully',
              _id: user._id
          });
      });
  }

]

/* Update User */
module.exports.update = [

  (req, res) => {

    let id = req.params.id;
    User.findOne({_id: id}, (err, user) => {
        if(err) {
            return res.status(500).json({
                success: false,
                message: 'Error saving record',
                error: err
            });
        }
        if(!user) {
            return res.status(404).json({
                success: false,
                message: 'No such record'
            });
        }

        // initialize record
        user.first_name =  req.body.first_name ? req.body.first_name : user.first_name;
        user.last_name =  req.body.last_name ? req.body.last_name : user.last_name;
        user.email =  req.body.email ? req.body.email : user.email;
        user.password =  req.body.password ? bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10)) : user.password;
        user.address =  req.body.address ? req.body.address : user.address;
        user.phone =  req.body.phone ? req.body.phone : user.phone;

        // save record
        user.save((err, user) => {
            if(err) {
                return res.status(500).json({
                    success: false,
                    message: 'Error getting record.'
                });
            }
            if(!user) {
                return res.status(404).json({
                    success: false,
                    message: 'No such record'
                });
            }
            return res.json({
              success: true,
              message: 'user data updated successfully',
              user: user
          });
        });
    });
  }
]

/* Delete User */
module.exports.destroy = (req, res) => {
  let id = req.params.id;
  User.findByIdAndRemove(id, (err, user) => {
      if(err) {
          return res.status(500).json({
              success: false,
              message: 'Error getting record.'
          });
      }
      return res.json({
          success: true,
          message: 'user data deleted successfully',
          _id: user._id
      });
  });

}

